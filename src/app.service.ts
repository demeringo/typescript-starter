import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  // iterative fib (efficient)
  iterative_fibonacci(element): number {
    const sequence = [0, 1];
    for (let i = 2; i <= element; i++) {
      sequence[i] = sequence[i - 2] + sequence[i - 1];
    }
    return sequence[element];
  }

  // recursive fib (less efficient)
  fibonacci(element): number {
    if (element === 0) return 0;
    if (element === 1) return 1;

    return this.fibonacci(element - 2) + this.fibonacci(element - 1);
  }

  getHello(): string {
    return 'Hello World! ' + this.iterative_fibonacci(46);
  }
}
